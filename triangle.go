package main

import (
  "math"
)

type Triangle struct {
  Normal Vertex

  V1 Vertex
  V2 Vertex
  V3 Vertex
}

func (t Triangle) GetBounds() Bounds {
  allX := []float64{t.V1.X, t.V2.X, t.V3.X}
  allY := []float64{t.V1.Y, t.V2.Y, t.V3.Y}
  allZ := []float64{t.V1.Z, t.V2.Z, t.V3.Z}

  return Bounds{
    MinX: getMin(allX) + t.Normal.X,
    MinY: getMin(allY) + t.Normal.Y,
    MinZ: getMin(allZ) + t.Normal.Z,

    MaxX: getMax(allX) + t.Normal.X,
    MaxY: getMax(allY) + t.Normal.Y,
    MaxZ: getMax(allZ) + t.Normal.Z,
  }
}

func getMin(s []float64) float64 {
  min := s[0]
  for _, v := range s {
    if v < min {
      min = v
    }
  }

  return min
}

func getMax(s []float64) float64 {
  max := s[0]
  for _, v := range s {
    if v > max {
      max = v
    }
  }

  return max
}

func (t Triangle) GetSurfaceArea() float64 {
  d1 := distance(t.V1, t.V2)
  d2 := distance(t.V1, t.V3)
  d3 := distance(t.V2, t.V3)

  p := (d1 + d2 + d3)/2

  sa := math.Sqrt(p*(p-d1)*(p-d2)*(p-d3))

  return sa
}

func distance(v1, v2 Vertex) float64 {
  x := math.Pow((v1.X - v2.X), 2)
  y := math.Pow((v1.Y - v2.Y), 2)
  z := math.Pow((v1.Z - v2.Z), 2)

  return math.Sqrt(x+y+z)
}
