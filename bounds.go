package main

type Bounds struct {
  MinX float64
  MinY float64
  MinZ float64

  MaxX float64
  MaxY float64
  MaxZ float64
}
