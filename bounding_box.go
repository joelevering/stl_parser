package main

type BoundingBox struct {
  Bounds Bounds
}

func (bb *BoundingBox) Update(b Bounds) {
  if b.MinX < bb.Bounds.MinX {
    bb.Bounds.MinX = b.MinX
  }

  if b.MinY < bb.Bounds.MinY {
    bb.Bounds.MinY = b.MinY
  }

  if b.MinZ < bb.Bounds.MinZ {
    bb.Bounds.MinZ = b.MinZ
  }

  if b.MaxX > bb.Bounds.MaxX {
    bb.Bounds.MaxX = b.MaxX
  }

  if b.MaxY > bb.Bounds.MaxY {
    bb.Bounds.MaxY = b.MaxY
  }

  if b.MaxZ > bb.Bounds.MaxZ {
    bb.Bounds.MaxZ = b.MaxZ
  }
}

func (bb *BoundingBox) GetCoordinates() []Vertex {
  return []Vertex{
    Vertex{
      X: bb.Bounds.MinX,
      Y: bb.Bounds.MinY,
      Z: bb.Bounds.MinZ,
    },
    Vertex{
      X: bb.Bounds.MinX,
      Y: bb.Bounds.MinY,
      Z: bb.Bounds.MaxZ,
    },
    Vertex{
      X: bb.Bounds.MinX,
      Y: bb.Bounds.MaxY,
      Z: bb.Bounds.MinZ,
    },
    Vertex{
      X: bb.Bounds.MinX,
      Y: bb.Bounds.MaxY,
      Z: bb.Bounds.MaxZ,
    },
    Vertex{
      X: bb.Bounds.MaxX,
      Y: bb.Bounds.MaxY,
      Z: bb.Bounds.MaxZ,
    },
    Vertex{
      X: bb.Bounds.MaxX,
      Y: bb.Bounds.MaxY,
      Z: bb.Bounds.MinZ,
    },
    Vertex{
      X: bb.Bounds.MaxX,
      Y: bb.Bounds.MinY,
      Z: bb.Bounds.MaxZ,
    },
    Vertex{
      X: bb.Bounds.MaxX,
      Y: bb.Bounds.MinY,
      Z: bb.Bounds.MinZ,
    },
  }
}
