# stl_parser

## Running This Code

* Install Go https://golang.org/doc/install
* In terminal run `go get gitlab.com/joelevering/stl_parser`
* Go to the `stl_parser` directory `cd ~/go/src/gitlab.com/joelevering/stl_parser`
* Run the program with an argument for filename e.g. `./stl_parser files/moon.stl`

## Design Decisions

* I chose Go as the language for this project. The three languages I considered were Ruby, Elixir, and Go.
  * Ruby is easy to use and the team at Fast Radius is familiar with it. I think that team familiarity with a language is extremely important, but Ruby is also by the far the least efficient language in contention, so I decided not to use it.
  * Elixir is significantly faster than Ruby and, to my knowledge, is the primary language used at Fast Radius. These benefits are fantastic, but I'm not personally well-versed in Elixir. As impressive as writing this program in Elixir would have been, I decided the risk of writing simple errors or inefficient code wasn't worth choosing Elixir.
  * Go is also significantly faster than Ruby and is a language I'm quite comfortable writing. It's suited to writing simple command-line apps, it is relatively easy to understand, and its built-in concurrency offers a path forward when/if this code needs to be improved for better performance. I also feel like Go gives me the best chance to represent my abilities.
* I chose not to store information on each triangle (in-memory or otherwise).
  * I originally assumed building up an array of all the triangles would be necessary, but none of the requirements necessitate storing triangle information.
  * Analyzing the triangles and storing relevant data (e.g. surface area) fulfills the program requirements without wasting resources.
* I'm reading the file with a buffered reader instead of reading it all at once, which should scale well with much larger files.
* I wrote separate files/classes for both the triangles and the bounding box calculations. There are also structs built for vertices and 3d bounds. These are split out to make the code more readable and facilitate code extension in the future. It also compartmentalizes code so it doesn't start bleeding into areas without deliberate change (e.g. the `distance()` method is private and in `triangle.go`)
* I didn't write tests here. I'd usually write tests for any code written, but since this is a compiled language there is built-in error checking on compilation and I have a file to run through the program on compilation to assure behavior hasn't changed.

## Performance Improvements

* I don't use any concurrency. It is very easy to call methods async in Go, so it's likely performance can be improved by asynchronously building and calculating stats on triangles as each few lines of the file are read.
* There are some areas where I initialize slices (varying-length arrays, basically), but could likely determine and set the length appropriately. This might give a small improvement to performance.
* It's possible there are more efficient ways to read the file. I use a `ReadLine()` method to skip over lines that aren't relevant (since we're assuming files are formatted correctly) I'm sure there's a way to jump to the next instance of a string, which might be more efficient.
* Right now the `GetBounds()` method on `Triangle` loops through all X, Y, and Z coordinates twice each. It'd be pretty easy to reduce that to once each by checking for min and max in the same method.

## Mistakes

* I forgot to commit after my true "first pass"... I had an iteration earlier than the first one committed which did not calculate bounding box and used a hardcoded filename
* I spent more time than necessary researching both the necessary math and learning about STL files themselves without reaching out for assistance. I think my reticence was in large part based on email being the only available method of communication. In a true shared work environment, I would have asked for a quick meeting for context on the problem from someone more knowledgable to save time and effort.


## Benchmarks

| file name    | # triangles | time         | time per triangle |
|--------------|-------------|--------------|-------------------|
| simple.stl   | 2           | 47.686µs     | 23.843µs          |
| moon.stl     | 116         | 804.884µs    | 6.9386µs          |
| courroie.stl | 9268        | 39.946727ms  | 4.3101µs          |
| frame_R.stl  | 75385       | 289.846481ms | 3.8448µs          |
