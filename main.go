package main

import(
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
  "time"
)

func main() {
  if len(os.Args) != 2 {
    panic("Please provide a filename!")
  }

  f, err := os.Open(os.Args[1])
  panicOnErr(err)

  var tcount int
  var sa float64
  var bb BoundingBox

  start := time.Now()

  reader := bufio.NewReader(f)

  name := nextLineWords(reader)[1]

  var t *Triangle

  for true {
    nw := nextLineWords(reader)
    if nw[0] == "endsolid" {
      f.Close()
      fmt.Println(fmt.Sprintf("Calculations for %s completed in %v", name, time.Now().Sub(start)))
      fmt.Println(fmt.Sprintf("Total Triangles: %d", tcount))
      fmt.Println(fmt.Sprintf("Surface Area: %f", sa))
      fmt.Println("Bounding Box Coordinates:")
      for _, v := range bb.GetCoordinates() {
        fmt.Println(v)
      }
      break
    }

    reader.ReadLine() // outer loop
    v1w := nextLineWords(reader)
    v2w := nextLineWords(reader)
    v3w := nextLineWords(reader)

    t = parseTriangle(nw, v1w, v2w, v3w)

    tcount += 1
    sa += t.GetSurfaceArea()
    bb.Update(t.GetBounds())

    reader.ReadLine() // endloop
    reader.ReadLine() // endfacet
  }
}

func parseTriangle(nw, v1w, v2w, v3w []string) *Triangle {
  if v1w[1] == "" {
    fmt.Println(nw)
    fmt.Println(v1w)
    fmt.Println(v2w)
    fmt.Println(v3w)
  }
  return &Triangle{
    Normal: Vertex{
      X: parseFloat(nw[2]),
      Y: parseFloat(nw[3]),
      Z: parseFloat(nw[4]),
    },
    V1: Vertex{
      X: parseFloat(v1w[1]),
      Y: parseFloat(v1w[2]),
      Z: parseFloat(v1w[3]),
    },
    V2: Vertex{
      X: parseFloat(v2w[1]),
      Y: parseFloat(v2w[2]),
      Z: parseFloat(v2w[3]),
    },
    V3:  Vertex{
      X: parseFloat(v3w[1]),
      Y: parseFloat(v3w[2]),
      Z: parseFloat(v3w[3]),
    },
  }
}

func nextLineWords(r *bufio.Reader) []string {
  line, err := r.ReadString('\n')
  panicOnErr(err)

  trimmed := strings.TrimSpace(line)
  split := strings.Split(trimmed, " ")

  // Some larger files I was testing from online have additional spaces between 'vertex' and the coordinates.
  // This cleans the slice of strings so that it doesn't have blank strings in it
  var res []string
  for _, s := range split {
    if s != "" {
      res = append(res, s)
    }
  }

  return res
}

func parseFloat(s string) float64 {
  f, err := strconv.ParseFloat(s, 64)
  panicOnErr(err)

  return f
}

func panicOnErr(e error) {
  if e != nil {
    panic(e)
  }
}
